#include "vex.h"

using namespace vex;

// A global instance of brain used for printing to the V5 brain screen
brain Brain;

// Devices
motor leftf(PORT2);
motor leftb(PORT11);
motor rightf(PORT10, 1);
motor rightb(PORT20, 1);
motor_group leftc(leftf, leftb);
motor_group rightc(rightf, rightb);
encoder leftEnc(Brain.ThreeWirePort.A);
encoder rightEnc(Brain.ThreeWirePort.H);


void vexcodeInit(void) {
  // Nothing to initialize
}