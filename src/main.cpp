/*----------------------------------------------------------------------------*/
/*                                                                            */
/*    Module:       main.cpp                                                  */
/*    Author:       C:\Users\Zayd                                             */
/*    Created:      Wed Mar 11 2020                                           */
/*    Description:  V5 project                                                */
/*                                                                            */
/*----------------------------------------------------------------------------*/

#include "vex.h"

using namespace vex;

double wheelTravel = 12.566;
double chassisRL = 15; //change according to chassis size


// test ZAYD AND BILAL ARE THE BEST!!!
void EncoderClear() {
  leftc.resetRotation();
  rightc.resetRotation();
}

void RightTurn(double rad, double deg, double time) {
  double rSpeed = 0;
  double lSpeed = 0;

  EncoderClear();
  double rTravel = (2 * 3.141 * rad) * (deg / 360);
  double lTravel = (2 * 3.141 * (rad + chassisRL)) * (deg / 360);

  while (rTravel < (rightc.rotation(rotationUnits::rev) * wheelTravel) || lTravel < (leftc.rotation(rotationUnits::rev) * wheelTravel)) {
    if (time > 0) {

      double rRate = (rTravel - (rightc.rotation(rotationUnits::rev) * wheelTravel))/time; //inches/msec
      double lRate = (lTravel - (leftc.rotation(rotationUnits::rev) * wheelTravel))/time;

      rSpeed = (rRate * 1000 * 60) / wheelTravel; //convert back to rpm
      lSpeed = (lRate * 1000 * 60) / wheelTravel;

    }
    else {

    }
    
    rightc.spin(fwd, rSpeed, rpm);
    leftc.spin(fwd, lSpeed, rpm);

    time = time - 10;
    task::sleep(10);
  }

}

void Drive(double dis, double time) {

  EncoderClear();
  double rSpeed = 0;
  double lSpeed = 0;

    while (dis < (rightc.rotation(rotationUnits::rev) * wheelTravel) || dis < (leftc.rotation(rotationUnits::rev) * wheelTravel)) {
    if (time > 0) {

      double rRate = (dis - (rightc.rotation(rotationUnits::rev) * wheelTravel))/time; //inches/msec
      double lRate = (dis - (leftc.rotation(rotationUnits::rev) * wheelTravel))/time;

      rSpeed = (rRate * 1000 * 60) / wheelTravel; //convert back to rpm
      lSpeed = (lRate * 1000 * 60) / wheelTravel;

    }
    else {

    }
    
    rightc.spin(fwd, rSpeed, rpm);
    leftc.spin(fwd, lSpeed, rpm);

    time = time - 10;
    task::sleep(10);
  }

}

int main() {
  // Initializing Robot Configuration. DO NOT REMOVE!
  vexcodeInit();
  
  Drive(12, 5000);
  RightTurn(5, 90, 5000);

}                               