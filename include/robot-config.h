using namespace vex;

extern brain Brain;

// VEXcode devices
extern motor leftf;
extern motor leftb;
extern motor rightf;
extern motor rightb;
extern motor_group leftc;
extern motor_group rightc;
extern encoder leftEnc;
extern encoder rightEnc;

/**
 * Used to initialize code/tasks/devices added using tools in VEXcode Text.
 * 
 * This should be called at the start of your int main function.
 */
void  vexcodeInit( void );